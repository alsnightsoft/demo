package edu.pucmm.controllers;

import edu.pucmm.domains.Student;
import edu.pucmm.services.StudentService;
import edu.pucmm.utils.Constants;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
public class HelloController {

    // Esto da un warning debido a que la industria esta cambiando.
    @Autowired
    private StudentService studentService;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    /*
        Visitar http://localhost:8087/students
     */
    @RequestMapping("/students")
    public String allStudents() {
        return Constants.stringify(new AllStudents(studentService.all()));
    }

    @RequestMapping("/console")
    public String consoleExample() {
        try {
            // Aqui usando la libreria me conecto por get y obtengo el JSON que me interesa
            String json = Jsoup.connect("http://127.0.0.1:8087/students").get().text();
            System.out.println("Este es el JSON leido del servicio: " + json);
            AllStudents allStudents = Constants.convert(json, AllStudents.class);
            System.out.println("Voy a listar los datos leidos del servicio");
            if (allStudents != null) {
                for (Student student : allStudents.getStudents()) {
                    System.out.println(student.toRow()); // Uso metodo toRow para imprimirlos de forma diferente al json.
                }
            } else {
                System.out.println("Tengo null en mi carga de estudiantes.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "See console";
    }

    @RequestMapping("/json")
    public String jsonExample() {
        Student student = new Student();
        student.setId(500000);
        student.setNames("Hola");
        student.setLastnames("Mundo");
        student.setBirthday(new Date());
        System.out.println(Constants.stringify(student));
        return "See console";
    }

    /**
     * Esta clase es solo para no tener problemas al parsear el JSON y estar comodos siempre
     */
    private class AllStudents {

        private List<Student> students;

        public AllStudents(List<Student> students) {
            this.students = students;
        }

        public List<Student> getStudents() {
            return students;
        }

        public void setStudents(List<Student> students) {
            this.students = students;
        }
    }
}
