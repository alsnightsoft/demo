package edu.pucmm.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {

    public static final String SEPARATOR = "|";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat(DATE_FORMAT).create();

    public static Date parseDate(String date) {
        try {
            return SIMPLE_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String formatDate(Date date) {
        return SIMPLE_DATE_FORMAT.format(date);
    }

    public static String stringify(Object object) {
        return gson.toJson(object);
    }

    public static <T> T convert(String data, Class<T> validClass) {
        try {
            return gson.fromJson(data, validClass);
        } catch (Exception ex) {
            return null;
        }
    }
}
