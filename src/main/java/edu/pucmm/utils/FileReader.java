package edu.pucmm.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase auxiliar para leer los archivos y no escribir eso en el servicio
 */
public class FileReader {

    public List<String> readFileStudents() {
        try {
            return Files.readAllLines(new File(getClass().getResource("/db/students.dat").getFile()).toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
