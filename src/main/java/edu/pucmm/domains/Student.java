package edu.pucmm.domains;

import edu.pucmm.utils.Constants;

import java.util.Date;

/**
 * La fecha es YYYY-MM-DD
 */
public class Student {

    private long id;
    private String names;
    private String lastnames;
    private Date birthday; // 2017-07-25

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void transform(String student) {
        String[] split = student.split("\\|");
        id = Long.parseLong(split[0]);
        names = split[1];
        lastnames = split[2];
        birthday = Constants.parseDate(split[3]);
    }

    public String toRow() {
        return id + Constants.SEPARATOR +
                names + Constants.SEPARATOR +
                lastnames + Constants.SEPARATOR +
                Constants.formatDate(birthday);
    }

    public String toJson() {
        return Constants.stringify(this);
    }
}
