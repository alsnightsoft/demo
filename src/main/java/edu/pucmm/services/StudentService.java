package edu.pucmm.services;

import edu.pucmm.domains.Student;
import edu.pucmm.utils.FileReader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("studentService")
public class StudentService {

    private static final FileReader FILE_READER = new FileReader();

    public List<Student> all() {
        List<Student> students = new ArrayList<>();
        for (String studentRow : FILE_READER.readFileStudents()) {
            Student student = new Student();
            student.transform(studentRow);
            students.add(student);
        }
        return students;
    }
}
